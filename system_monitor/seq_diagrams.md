# Sequence diagrams

## <u>CPU Monitor</u>

![CPU Monitor](/uploads/ba3b1931145bfa96935d99ddb77022b6/seq_cpu_monitor.png)

## <u>HDD Monitor</u>

![HDD Monitor](/uploads/87956c020182fe01218f718e27804d69/seq_hdd_monitor.png)

## <u>Memory Monitor</u>

![Memory Monitor](/uploads/88f2cc7762dd1a8560a61150b36510ab/seq_mem_monitor.png)

## <u>Net Monitor</u>

![Net Monitor](/uploads/5f59297b22678f4751d29b99c7badf43/seq_net_monitor.png)

## <u>NTP Monitor</u>

![NTP Monitor](/uploads/4001dbad4d2d50306e91b2daa5d1ecb0/seq_ntp_monitor.png)

## <u>Process Monitor</u>

![Process Monitor](/uploads/b376545a24ffd719292e2ef3a4d0a710/seq_process_monitor.png)

## <u>GPU Monitor</u>

![GPU Monitor](/uploads/7e208082fa4e1896cd0ec0effcb8da56/seq_gpu_monitor.png)
