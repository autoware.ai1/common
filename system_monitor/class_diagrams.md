# Class diagrams

## <u>CPU Monitor</u>

![CPU Monitor](/uploads/d2bff8ae905fc4173be178eb45d2796c/class_cpu_monitor.png)

## <u>HDD Monitor</u>

![HDD Monitor](/uploads/9a5848ac73e508ed962f82843e295cb6/class_hdd_monitor.png)

## <u>Memory Monitor</u>

![Memory Monitor](/uploads/61ee1e318809d36c4e3c172b5314801f/class_mem_monitor.png)

## <u>Net Monitor</u>

![Net Monitor](/uploads/386ac48f17151a151631071acbdbb254/class_net_monitor.png)

## <u>NTP Monitor</u>

![NTP Monitor](/uploads/0eb55c437aa30f922e053421f8cd5b19/class_ntp_monitor.png)

## <u>Process Monitor</u>

![Process Monitor](/uploads/a04efa6cd52fa3c67c3d9660b8c8242a/class_process_monitor.png)

## <u>GPU Monitor</u>

![GPU Monitor](/uploads/883cd8b519677cb702a3fb44c63877a4/class_gpu_monitor.png)
